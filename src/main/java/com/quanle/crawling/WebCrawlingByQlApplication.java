package com.quanle.crawling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebCrawlingByQlApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebCrawlingByQlApplication.class, args);
	}
}
